console.log('expressions files has been loaded');

var someNumber = 10;
var someValue = 'someValue';
var someEmptyValue = '';
var flag = true;
var someZeroNumber = 0;

if (someNumber > 10) { //false
    console.log('someNumber is greater then 10');
}

if (someNumber >= 10) { //true
    console.log('someNumber is greater OR equal to 10');
}

if (someNumber === 10) { //true
    console.log('someNumber equal to 10');
}

if (flag === true) { //true
    console.log('flag is true');
}

if (flag) { //true
    console.log('flag is true');
}

if (someZeroNumber) { //false - 0 is falsy
    console.log('someZeroNumber is true');
}

if (someValue) { //true
    console.log('someValue is true');
}

if (someEmptyValue) { //false
    console.log('someEmptyValue is true');
}

if (!someEmptyValue) { //true
    console.log('!someEmptyValue');
}

if (someValue && flag) { //true
    console.log('someValue && flag');
}

if (2 > 1 && 3 < 4 || 1 === 2) { //?
    console.log('condition true');
}



//if x is greater then 15 AND smaller then 22 -- OR y is true

function validate(x, y) {
    console.log(x, y);

    if ((x > 15 && x < 22) || y) {
        console.log('this is true');
    } else {
        console.log('this is false');
    }
}


validate(17, true);
validate(17, false);
validate(10, false);

