
function recursionWithLoop(num) {
    while (num > 0) {
        console.log(num);
        num--;
    }
}

function recursion(num) {
    console.log(num);

    const newNumber = num - 1;
    if (newNumber === 0) {
        console.log('stop');
    } else {
        recursion(newNumber);
    }
}

function devider(num, count) {
    const newNumber = num / 2.5;
    if (newNumber <= 0) {
        console.log('stop', count);
    } else {
        devider(newNumber, count + 1);
    }
}

/*
const startingValue = 10;
let j = startingValue;
let dividens = 0;
while (j > 0) { //
    j = j / 2.5;
    //console.log(j);
    dividens++;
    console.log(dividens); //816
}

*/


//devider(10, 1);





