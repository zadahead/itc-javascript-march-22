
//one dimens array
const list = [1, 2, 3, 4];

//two dimens array
const matrix = [
    [1, 2, 3], //i
    [4, 5, 6], //i
    [7, 8, 9], //i
    [10, 11, 12, 13] //i
];

function loopInMatrix() {
    for (let i = 0; i < matrix.length; i++) {
        const list = matrix[i];

        for (let j = 0; j < list.length; j++) {
            const item = matrix[j];
            console.log(item);
        }
    }
}

/*
1 
2 
3 
4 
5 
6 
7 
8 
9 
10 
11 
12 
13 
*/

loopInMatrix();