
const list = [1, 2, 3, 4];

//I know many iterations I need
for (let i = 0; i < list.length; i++) {
    const item = list[i];
    console.log(item);
}

for (let i = 0; i < list.length; i++) {
    if (i === 2) {
        break;
    }
    console.log(i);
}

//I will figure out as I go how many I need
let i = 0;
while (i < list.length) {
    const item = list[i];
    console.log(item);
    i++;
}


const startingValue = 10;

let j = startingValue;
let dividens = 0;
while (j > 0) { //
    j = j / 2.5;
    //console.log(j);
    dividens++;
    console.log(dividens); //816
}

console.log('while dividens', dividens);


let x = 10;
for (let dividens = 0; x > 0; dividens++) {
    x = x / 2.5;
    //console.log(x);
    console.log(dividens); //

}

console.log('for dividens', dividens);


let flag = 0;
while (flag < 10) {
    if (flag === 5) {
        break;
    }
    console.log('flag', flag);
    flag++;
}



let flag2 = 0;
while (flag2 < 10) {
    flag2++;
    console.log('flag', flag2);
}

do {
    flag2++;
    console.log('second flag', flag2);
}
while (flag2 < 10);









