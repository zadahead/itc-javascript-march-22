console.log('switch page');

function switchThis(name) {

    switch (name) {
        case 'mosh':
        case 'another mosh':
            return 'Hello Mosh';
        case 'david':
            return 'Bye David';
        default:
            return 'who are you?';
    }
}

/*
    create a function that expect a name
    switch between names with this logic
    mosh => hello mosh
    david => bye david
    all other => who are you?
*/

const rtnValue = switchThis('fhgfd');
console.log(rtnValue);

function cal(a, b) {
    return a + b;
}

const res = cal(10, 5);
const res2 = 10 + 5;




