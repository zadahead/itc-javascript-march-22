

//state
const options = {
    filter: '',
    sort: 0
}

async function displaydata() {
    let url = 'https://jsonplaceholder.typicode.com/users';

    let response = await fetch(url);
    let users = await response.json();

    //filter
    const filteredList = users.filter((user) => {
        if (!options.filter) { return true }

        if (user.name.toLowerCase().includes(options.filter.toLowerCase())) {
            return true;
        }
        return false;
    })


    //sort
    if (options.sort) { //

        filteredList.sort((a, b) => {
            if (a.name > b.name) {
                return options.sort;
            }
            return options.sort * -1;
        })
    }


    //log user Info

    const mappedList = filteredList.map((user) => (
        {
            id: user.id,
            name: user.name
        }
    ))

    for (user of mappedList) {
        const userInfo = await fetchMoreInfo(user.id);
        user.username = userInfo.username;

        console.log(userInfo);
    }


    console.log(mappedList);

    document.getElementById('root').replaceChildren();

    for (let user of mappedList) {
        const div = document.createElement('div');
        div.innerHTML = `${user.name} (${user.username})`;
        div.classList.add('user');
        document.getElementById('root').appendChild(div);
    }
}

const fetchMoreInfo = async (userId) => {
    let url = 'https://jsonplaceholder.typicode.com/users/' + userId;

    let response = await fetch(url);
    let userInfo = await response.json();

    return userInfo;
}

displaydata();

const handleFilter = () => {
    const value = document.getElementById('filter').value;
    console.log('click filter', value);

    options.filter = value;

    displaydata();
}

const sort = (dir) => {
    options.sort = dir;
    displaydata();
}

const ascending = () => {
    sort(1);
}

const decending = () => {
    sort(-1);
}



