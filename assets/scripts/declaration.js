console.log('declaration is loaded');

/*
    create a function that recieves a number (example 5)
    the number respresent 

    5 * 5 * 5 * 5 .... 10 times //easier

    5 * 5 + 5 / 5 - 5 .... 10 times //harder

    return the result.

*/

//redeclare
//update

//var -- redeclare + update
//let -- update
//const -- 


//console.log(someVariable); //Error

function declareVar() {
    //hoisting
    var someVariable;
    //

    console.log(someVariable); //undefined
    //console.log(blabla); //Error!

    var someVariable = 'value';
    console.log(someVariable); //value

    var someVariable = 'anotherValue';
    console.log(someVariable); //anotherValue
}

function declareLet() {
    //console.log(someLetVariable); //Error

    let someLetVariable = 'value';
    console.log(someLetVariable); //value

    someLetVariable = 'anotherValue';
    console.log(someLetVariable); //value
}

function decalreConst() {
    const someConstVariable = 'value';
    console.log(someConstVariable); //value

    someConstVariable = 'anotherValue';
    //console.log(someConstVariable); //Error!
}
var global = 1234;

function scope() {

    function inner() {
        for (var i = 0; i < 4; i++) {
            console.log(i);
        }
    }
    inner();
}



function calc1(num) {
    //5 * 5 * 5 * 5 .... 10 times //easier
    console.log('number ', num);
    const LENGTH = 3;
    let sum = 1;

    for (let i = 0; i < LENGTH; i++) {
        sum = sum * num;
    }

    console.log('sum', sum);
}

function toThePowerV2(num) {
    //5 * 5 + 5 / 5 - 5 .... 10 times //harder
    const times = 10;
    let res = 0;

    return res = times * (num + num - num / num * num);
    //%
    //if
}

function toThePowerV3(num) {
    const times = 10;
    let res = 0;
    let calc = (num * num + num / num - num)

    for (let i = 0; i < times; i++) {
        //when i === 0
        //res = res * 5

        //when i === 1
        //res = res + 5
        res += calc;
    }
    return res;
}


//TODO: go over this wonderful solution
function myPowHarder(number) {

    var result = number;
    const TIMES = 10;

    const operators = [
        function (a, b) { return a * b },
        function (a, b) { return a + b },
        function (a, b) { return a / b },
        function (a, b) { return a - b },

    ];

    for (let i = 0; i < TIMES; i++) {
        divider = i % 4;
        result = operators[divider](result, number);
    }

    return result;
}




function powerHard(num) {

    const LENGTH = 10;
    let sum = 0;


    for (let i = 0; i < LENGTH; i++) {

        if (i % 4 == 0) {

            sum = sum + (num * num + num / num);

        }

        if (LENGTH - i < 4) {
            sum = sum * num;
        }

        if (LENGTH - i < 3) {
            sum = sum + num;
        }

        if (LENGTH - i < 2) {
            sum = sum / num;
        }

        if (LENGTH - i < 1) {
            sum = sum - num;
        }


    }

    return sum;
}

function calc2(num) {
    const MOLTI = 10;
    let a = 5;
    for (let i = 1; i <= MOLTI; i++) {
        if (i % 4 === 1) {
            a = a * num;
        }
        else if (i % 4 === 2) {
            a = a + num;
        }
        else if (i % 4 === 3) {
            a = a - num;
        }
        else a = a / num;
    };
    return a
}

function calc(num) {
    //5 * 5 + 5 / 5 - 5 .... 10 times //harder
    const PREFIX = 3;
    var sum = 1;

    for (let i = 0; i < PREFIX; i++) {
        const expression = i % 4;

        switch (expression) { //0, 1, 2, 3
            case 0:
                sum = sum * num;
                break;
            case 1:
                sum = sum + num;
                break;
            case 2:
                sum = sum / num;
                break;
            case 3:
                sum = sum - num;
                break;
            default:
                break;
        }


    }

    console.log(sum);
}

scope();