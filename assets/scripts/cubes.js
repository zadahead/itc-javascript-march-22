
// here we can popup whatever we want :)

window.addEventListener('load', pageLoaded);

function pageLoaded() {
    console.log('page is loaded');

    var cubesList = [
        { //0
            innerHTML: 'aaa',
            backgroundColor: 'red',
            color: 'yellow'
        },
        { //1
            innerHTML: 'bbb',
            backgroundColor: 'green'
        }
    ]

    for (var i = 0; i < cubesList.length; i++) {
        var item = cubesList[i];
        addCube(item);
    }

}

function addCube(cube) {
    var myCube = document.createElement('div');
    myCube.classList.add('cube');
    myCube.innerHTML = cube.innerHTML;
    myCube.style.backgroundColor = cube.backgroundColor;
    myCube.style.color = cube.color || 'black';

    var cubes = document.querySelector('.cubes');
    cubes.append(myCube);
}


// here we can popup whatever we want :)     