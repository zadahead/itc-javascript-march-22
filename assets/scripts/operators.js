
console.log('operators files has been loaded');

var initialNumber = 0;
console.log(initialNumber);

initialNumber = 10;
console.log(initialNumber);

initialNumber = initialNumber + 20; //30
console.log(initialNumber);

initialNumber = initialNumber - 10; //20
console.log(initialNumber);

initialNumber = initialNumber * 2; //40
console.log(initialNumber);

initialNumber = initialNumber / 4; //10
console.log(initialNumber);


initialNumber = initialNumber % 5; //0
console.log(initialNumber);

initialNumber++; //1
console.log(initialNumber);

initialNumber--; //0
console.log(initialNumber);

++initialNumber; //1
console.log(initialNumber);

--initialNumber; //0
console.log(initialNumber);




function numPlusPlus(num) { //initialNumber++
    var prevNum = num;
    num = num + 1;
    return prevNum;
}

function plusPlusNum(num) { //++initialNumber
    num = num + 1;
    return num;
}

