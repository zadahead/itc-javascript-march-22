
//loop over arrays

const list = [1, 2, 3, 4];

function basicForLoop() {
    for (let i = 0; i < list.length; i++) {
        const item = list[i];
        console.log(i, item);
    }
}

function forEachLoop() {
    list.forEach(function (item, i) {
        console.log(i, item);
    });
}

function forOfLoop() {
    for (let item of list) {
        console.log(item);
    }
}


// loop through an object

const obj = {
    id: 1,
    name: 'mosh',
    age: 25
}

function forInLoop() {
    for (let key in obj) {
        console.log(key, obj[key]);
    }

    console.log('id', obj['id']);
    console.log('name', obj['name']);
    console.log('age', obj['age']);

    console.log('id', obj.id);
    console.log('name', obj.name);
    console.log('age', obj.age);
}

//console.log(obj.name);
//console.log(obj['name']);

/* try to log that

id: 1
name: mosh
age: 25


*/


forInLoop();