/*

    1) some number => 5

    2) async add 10 to 5

    3) async multiply result by 4

    4) async divide result by 3

    5) console.log(result)

*/

const add = (num) => {
    return new Promise((res) => {
        setTimeout(() => {
            res(10 + num)
        }, 1000);
    })
}

const multiply = (num) => {
    return new Promise((res) => {
        setTimeout(() => {
            res(4 * num);
        }, 1000);
    })
}

const divide = (num) => {
    return new Promise((res) => {
        setTimeout(() => {
            res(num / 3);
        }, 1000);
    })
}


const startJourney = (num) => {
    //add
    add(num).then((addedNumber) => {
        console.log(addedNumber);
        //multi
        multiply(addedNumber).then((multipliedNumber) => {
            console.log(multipliedNumber);

            //divide
            divide(multipliedNumber).then((result) => {
                console.log('result => ', result);
            })
        })
    })
}


const startJourneyAwait = async (num) => {
    const addedNumber = await add(num);
    console.log(addedNumber);

    let multipliedNumber = await multiply(addedNumber);

    const result = await divide(multipliedNumber);

    console.log('result => ', result);
}




startJourneyAwait(10);





async function showAvatar() {
    let resp = await fetch('/article/promise-chaining/user.json');
    let user = await resp.json();

    let gihubResp = await fetch(`https://api.github.com/users/${user.name}`)
    let gitHubUser = await gihubResp.json();

    let img = document.createElement('img');
    img.src = gitHubUser.avatar_url;
    img.className = "asdasdasdsa";

    document.body.append(img);


    const promise = (res, rej) => {
        setTimeout(() => {
            res();
        }, 3000)
    }

    await new Promise(promise);

    img.remove();

    return gitHubUser;
}

showAvatar();


