function recap() {
    try {
        console.log('do 1');
        console.log('do 2');
        console.log(mosh);
        console.log('do 3');
        console.log('do 4');
    } catch (error) {
        console.log('error');
    }
}


function recapAsync() {
    console.log('recapAsync start');

    function onEnd() {
        console.log('recapAsync end');
    }

    function onError() {
        console.log('recapAsync ERROR');
    }

    function onFinal() {
        console.log('recapAsync Final');
    }


    ///asyncFunc(onEnd, onError);

    //asyncFunc().then(onEnd).catch(onError);

    asyncFunc()
        .then(function () {
            onEnd();
            console.log('blabla');
        })
        .catch(onError)
        .finally(onFinal);
}

function asyncFunc() {
    //create the action
    function action(res, rej) {
        setTimeout(() => {
            try {
                console.log('do 1');
                console.log('do 2');
                console.log(mosh);
                console.log('do 3');
                console.log('do 4');

                //create a new Promise
                res();
            } catch (error) {
                console.log('error', error.message);
                rej();
            } finally {
                console.log('finally');
            }
        }, 2000);
    }

    //trigger the action

    return new Promise(action)
}


/*

sync
console.log('recapAsync start');
asyncFunc();
console.log('recapAsync end');

async
setTimeout (inside)

sync
try
console.log('do 1');
console.log('do 2');
catch
console.log('error');


*/

//recapAsync();



function callFetch() {


    function onSecondThen(response) {
        console.log(response.data);
    }

    function onError(err) {
        console.log(err);
        console.log('ERROR ACCURED', err.message);
    }

    function onFinal() {
        console.log('finally');
    }

    axios.get('https://jsonplaceholder.typicode.com/todos/1')
        .then(onSecondThen)
        .catch(onError)
        .finally(onFinal)
}

callFetch();



