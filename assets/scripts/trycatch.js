
/*

open a door 

search for something
calculating

close door

*/

function calc(x, y) {
    if (x < y) {
        throw Error('X must be smaller then Y');
    }
    return x + y;
}

function trythis() {
    try {
        console.log('do 1');
        console.log('do 2');

        const result = calc(5, 10);
        console.log(result);

        console.log('do 3');
        console.log('do 4');
    } catch (err) {
        console.log(err.message);
        document.getElementById('error-block').innerHTML = err.message;
    } finally {
        console.log('close all');
    }
}



trythis();
console.log('do 5');




