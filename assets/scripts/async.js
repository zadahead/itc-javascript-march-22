


function go() {
    console.log('add vegi');
    console.log('add snack');

    orderCheesePromise()
        .then(function (name) {
            console.log('add', name);
        })
        .catch(function () {
            console.log('something went wrong');
        });

    //orderCheese(onOK, onError);

    console.log('add drink');
    console.log('add tomato');
}


function orderCheese(callback, onerror) {

    setTimeout(() => {
        try {
            console.log('step 1');
            console.log('step 2');
            //console.log(mosh);
            callback('Cheese');
        } catch (error) {
            onerror();
        }
    }, 2000);

}


function orderCheesePromise() {

    function handler(res, rej) {
        setTimeout(() => {
            try {
                console.log('step 1');
                console.log('step 2');
                //console.log(mosh);
                res('Cheese');
            } catch (error) {
                rej();
            }
        }, 2000);
    }

    return new Promise(handler);
}







function heavyDuty() {
    let val = 2;
    for (let i = 0; i < 1000000000; i++) {
        val = val * 3;
    }
    console.log(val);
}



function fetchThis() {

    function onResponse(json) {
        console.log(json.data)
    }

    axios.get('https://jsonplaceholder.typicode.com/todos')
        .then(onResponse)
}

fetchThis();