


function createElement(location, array) {
    for (let i = 0; i < array.length; i++) {
        const btn = document.createElement('button');
        btn.classList.add(`btn`);
        btn.innerText = array[i];

        btn.addEventListener('click', action);

        document.querySelector(location).append(btn);
    }
}

const actionsArray = [`C`];
const digitsArray = [
    '7', '8', '9',
    '4', '5', '6',
    '1', '2', '3',
    '.', '0', '='
];

const expressionsArray = [`+`, `-`, `/`, `*`];

createElement(`.header .actions`, actionsArray);
createElement(`.container .digits`, digitsArray);
createElement(`.container .expressions`, expressionsArray);



const monitor = document.querySelector('.monitor');
let monitorValue;
let expression;

/*
563 + 897
*/


function action(e) {
    const key = e.target.innerText;

    console.log(key);

    switch (key) {
        case `+`:
        case `-`:
        case `/`:
        case `*`:
            monitorValue = monitor.innerText;
            expression = key;
            setValue();
            //handle expressions
            break;
        case `C`:
            //handle cancel
            clear();
            break;
        case `=`:
            //handle equal
            const result = eval(`${monitorValue} ${expression} ${monitor.innerText}`);
            monitor.innerHTML = result;
            break;
        case `.`:
            //handle dot
            setValue('.');
            break;
        default:
            //handle digits
            setValue(key);
            break;
    }

}

function setValue(value) {


    let currValue = monitor.innerText;

    if (currValue === "0") {
        currValue = '';
    }

    if (!value) {
        value = '';
        currValue = "0";
    }

    monitor.innerHTML = currValue + value;
}

function clear() {
    setValue();

    monitorValue = null;
    expression = null;
}


setValue('0');







