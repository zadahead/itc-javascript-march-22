const createElement = () => {
    const div = document.createElement('div');
    div.style.width = '50px';
    div.style.height = '50px';
    div.style.backgroundColor = 'red';

    return div;
}

const appendToContainer = (div) => {
    document.querySelector('.container').append(div);
}


class Elem {

    constructor() {
        this.create();
        this.bind();

        this.isTrash = false;
    }

    create() {
        const div = document.createElement('div');
        div.style.width = '50px';
        div.style.height = '50px';
        div.style.backgroundColor = 'red';

        this.div = div;
    }

    bind = () => {
        this.div.addEventListener('click', this.handleClick);
    }

    appendToContainer = () => {
        document.querySelector('.container').append(this.div);
        this.isTrash = false;
    }

    appendToTrash = () => {
        document.querySelector('.trash').append(this.div);
        this.isTrash = true;
    }

    write(text) {
        this.div.innerHTML = text;
    }

    handleClick = () => {
        this.write('hello');
        if (this.isTrash) {
            this.appendToContainer();
        } else {
            this.appendToTrash();
        }
    }
}

class Box extends Elem {
    constructor() {
        super();
        this.color();
    }

    color = () => {
        this.div.style.backgroundColor = 'blue';
    }

    write = () => {
        super.write('my text');
        console.log('yaaaaaaa');
    }
}

class Circle extends Elem {

    create() {
        super.create();
        console.log('hello');
        this.div.style.borderRadius = '50%';
    }
}


const addBox = () => {
    const box = new Box();
    box.appendToContainer();
}

const addCircle = () => {
    const circle = new Circle();
    circle.appendToContainer();
}




